<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="82"/>
        <source>Press space bar to start</source>
        <translation>Press space bar to start</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="106"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="108"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="111"/>
        <source>E&amp;xit Game</source>
        <translation>E&amp;xit Game</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="362"/>
        <source>Number of points:</source>
        <translation>Number of points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="426"/>
        <source>Points:</source>
        <translation>Points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="425"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="437"/>
        <source>Press space bar to proceed</source>
        <translation>Press space bar to proceed</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="77"/>
        <source> %</source>
        <translation> %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="108"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="109"/>
        <source>Target Errors</source>
        <translation>Target Errors</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="110"/>
        <source>Actual Errors</source>
        <translation>Actual Errors</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="111"/>
        <source>Frequency</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="112"/>
        <source>Error Rate</source>
        <translation>Error Rate</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="115"/>
        <source>This column shows all of the
characters typed</source>
        <translation>This column shows all of the
characters typed</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="119"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>The character was supposed to be typed, but wasn&apos;t</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="122"/>
        <source>Character was mistyped</source>
        <translation>Character was mistyped</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="124"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>This column indicates the total frequency of each
character shown</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="128"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="152"/>
        <source>Reset characters</source>
        <translation>Reset characters</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="234"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="240"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="240"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
</context>
<context>
    <name>DatabasePage</name>
    <message>
        <location filename="../widget/settingspages.cpp" line="392"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="371"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="387"/>
        <source>Data Location</source>
        <translation>Data Location</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="397"/>
        <source>If no file &quot;%1&quot; exists, a new, empty database will be created.</source>
        <translation>If no file &quot;%1&quot; exists, a new, empty database will be created.</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="402"/>
        <source>The path can not be changed in the portable version of %1.</source>
        <translation>The path can not be changed in the portable version of %1.</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="361"/>
        <source>User Data</source>
        <translation>User Data</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="364"/>
        <source>Reset &amp;completed lessons</source>
        <translation>Reset &amp;completed lessons</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="366"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation)</source>
        <translation>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation)</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="370"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>Reset all &amp;recorded characters</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="390"/>
        <source>Database directory &quot;%1&quot;:</source>
        <translation>Database directory &quot;%1&quot;:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="463"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="467"/>
        <location filename="../widget/settingspages.cpp" line="491"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="467"/>
        <location filename="../widget/settingspages.cpp" line="491"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="478"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>The data for completed lessons was successfully deleted!
</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="487"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="502"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>The recorded characters were successfully deleted!
</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="82"/>
        <source>The process will be aborted.</source>
        <translation>The process will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>The update will be aborted.</source>
        <translation>The update will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>The program will be aborted.</source>
        <translation>The program will be aborted.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot load the program logo.</source>
        <translation>Cannot load the program logo.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="102"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Cannot load the keyboard bitmap.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="105"/>
        <source>Cannot load the timer background.</source>
        <translation>Cannot load the timer background.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="108"/>
        <source>Cannot load the status bar background.</source>
        <translation>Cannot load the status bar background.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="113"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="118"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="124"/>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="131"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="138"/>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Connection to the database failed.</source>
        <translation>Connection to the database failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="145"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>The user table with lesson data  cannot be emptied.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="149"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>The user table with error data cannot be emptied.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="153"/>
        <source>No lessons exist.</source>
        <translation>No lessons exist.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="156"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>No lesson selected.
Please select a lesson.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="159"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Cannot create the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="163"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="178"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="207"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Cannot save the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="210"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Cannot retrieve the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="213"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Cannot analyze the lesson.
SQL statement failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="216"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>The file could not be imported.
Please check whether it is a readable text file.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="221"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="226"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>The file could not be exported.
Please check to see whether it is a writable text file.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="235"/>
        <source>Cannot create temporary file.</source>
        <translation>Cannot create temporary file.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="238"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Cannot execute the update process.
Please check your internet connection and proxy settings.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="242"/>
        <source>Cannot read the online update version.</source>
        <translation>Cannot read the online update version.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="245"/>
        <source>Cannot read the database update version.</source>
        <translation>Cannot read the database update version.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="248"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>Cannot execute the SQL statement.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Cannot find typing mistake definitions.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="254"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Cannot create temporary file.
Update failed.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="257"/>
        <source>Cannot create analysis table.</source>
        <translation>Cannot create analysis table.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="260"/>
        <source>Cannot create analysis index.</source>
        <translation>Cannot create analysis index.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="263"/>
        <source>Cannot fill analysis table with values.</source>
        <translation>Cannot fill analysis table with values.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="267"/>
        <source>An error has occured.</source>
        <translation>An error has occured.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="271"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Error number: %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="62"/>
        <source>Report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="66"/>
        <source>Overview of Lessons</source>
        <translation>Overview of Lessons</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="67"/>
        <source>Progress of Lessons</source>
        <translation>Progress of Lessons</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="68"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="69"/>
        <source>Fingers</source>
        <translation>Fingers</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="70"/>
        <source>Comparison Table</source>
        <translation>Comparison Table</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="89"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="90"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="173"/>
        <source>Use examples</source>
        <translation>Use examples</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="177"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="187"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="192"/>
        <source>For example, this equates to …</source>
        <translation>For example, this equates to …</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="197"/>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="204"/>
        <location filename="../widget/evaluationwidget.cpp" line="220"/>
        <location filename="../widget/evaluationwidget.cpp" line="231"/>
        <location filename="../widget/evaluationwidget.cpp" line="242"/>
        <location filename="../widget/evaluationwidget.cpp" line="258"/>
        <location filename="../widget/evaluationwidget.cpp" line="269"/>
        <location filename="../widget/evaluationwidget.cpp" line="280"/>
        <location filename="../widget/evaluationwidget.cpp" line="296"/>
        <location filename="../widget/evaluationwidget.cpp" line="307"/>
        <location filename="../widget/evaluationwidget.cpp" line="318"/>
        <location filename="../widget/evaluationwidget.cpp" line="334"/>
        <location filename="../widget/evaluationwidget.cpp" line="345"/>
        <location filename="../widget/evaluationwidget.cpp" line="356"/>
        <location filename="../widget/evaluationwidget.cpp" line="372"/>
        <location filename="../widget/evaluationwidget.cpp" line="383"/>
        <location filename="../widget/evaluationwidget.cpp" line="394"/>
        <location filename="../widget/evaluationwidget.cpp" line="410"/>
        <location filename="../widget/evaluationwidget.cpp" line="421"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="208"/>
        <location filename="../widget/evaluationwidget.cpp" line="224"/>
        <location filename="../widget/evaluationwidget.cpp" line="235"/>
        <location filename="../widget/evaluationwidget.cpp" line="246"/>
        <location filename="../widget/evaluationwidget.cpp" line="262"/>
        <location filename="../widget/evaluationwidget.cpp" line="273"/>
        <location filename="../widget/evaluationwidget.cpp" line="284"/>
        <location filename="../widget/evaluationwidget.cpp" line="300"/>
        <location filename="../widget/evaluationwidget.cpp" line="311"/>
        <location filename="../widget/evaluationwidget.cpp" line="322"/>
        <location filename="../widget/evaluationwidget.cpp" line="338"/>
        <location filename="../widget/evaluationwidget.cpp" line="349"/>
        <location filename="../widget/evaluationwidget.cpp" line="360"/>
        <location filename="../widget/evaluationwidget.cpp" line="376"/>
        <location filename="../widget/evaluationwidget.cpp" line="387"/>
        <location filename="../widget/evaluationwidget.cpp" line="398"/>
        <location filename="../widget/evaluationwidget.cpp" line="414"/>
        <location filename="../widget/evaluationwidget.cpp" line="425"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 cpm and %2 errors in %3 minutes</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="213"/>
        <source>No experience in touch typing</source>
        <translation>No experience in touch typing</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="251"/>
        <source>First steps in touch typing</source>
        <translation>First steps in touch typing</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="289"/>
        <source>Advanced level</source>
        <translation>Advanced level</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="327"/>
        <source>Suitable skills</source>
        <translation>Suitable skills</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="365"/>
        <source>Very good skills</source>
        <translation>Very good skills</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="403"/>
        <source>Perfect skills</source>
        <translation>Perfect skills</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="255"/>
        <source>Error rates of your fingers</source>
        <translation>Error rates of your fingers</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="262"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>The error rate is based on the recorded characters and the current selected keyboard layout.</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="272"/>
        <source>Error Rate:</source>
        <translation>Error Rate:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="274"/>
        <source>Frequency:</source>
        <translation>Frequency:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="276"/>
        <source>Errors:</source>
        <translation>Errors:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="42"/>
        <location filename="../widget/helpbrowser.cpp" line="77"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="54"/>
        <location filename="../widget/helpbrowser.cpp" line="61"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="89"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="92"/>
        <source>Table of Contents</source>
        <translation>Table of Contents</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="93"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="96"/>
        <source>&amp;Print page</source>
        <translation>&amp;Print page</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="136"/>
        <source>Print page</source>
        <translation>Print page</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="45"/>
        <source>Introduction</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="77"/>
        <source>Welcome to TIPP10</source>
        <translation>Welcome to TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="82"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="89"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Tips for using the 10 finger system</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="93"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="99"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="103"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="108"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="114"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Try to remain relaxed during the typing lessons.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="117"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="121"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="125"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>If you need assistance with using the software use the help function.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="136"/>
        <source>All rights reserved.</source>
        <translation>All rights reserved.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="150"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Launch TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="154"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>Do&amp;n&apos;t show me this window again</translation>
    </message>
</context>
<context>
    <name>LanguagePage</name>
    <message>
        <location filename="../widget/settingspages.cpp" line="730"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="732"/>
        <source>Keyboard Layout:</source>
        <translation>Keyboard Layout:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="738"/>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="741"/>
        <source>Training Lessons:</source>
        <translation>Training Lessons:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="757"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="122"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="123"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="124"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="132"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Name of the lesson (20 characters max.):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="134"/>
        <source>Short description (120 characters max.):</source>
        <translation>Short description (120 characters max.):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="135"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Lesson content (at least two lines):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="166"/>
        <source>Sentence Lesson</source>
        <translation>Sentence Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="168"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Every line will be completed with
a line break at the end</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="173"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="180"/>
        <source>Edit own Lesson</source>
        <translation>Edit own Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="252"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Please enter the name of the lesson
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="258"/>
        <source>Please enter entire lesson content
</source>
        <translation>Please enter entire lesson content
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="281"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Please enter 400 lines of text maximum
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="289"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>The name of the lesson already exists. Please enter a new lesson name.
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="171"/>
        <source>Word Lesson</source>
        <translation>Word Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="137"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="147"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="155"/>
        <source>Dictate the text as:</source>
        <translation>Dictate the text as:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="182"/>
        <source>Name of the Lesson:</source>
        <translation>Name of the Lesson:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="186"/>
        <source>Create own Lesson</source>
        <translation>Create own Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="275"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Please enter at least two lines of text
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="39"/>
        <source>Print Lesson</source>
        <translation>Print Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="62"/>
        <source>&amp;Print</source>
        <translation>&amp;Print</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="63"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="76"/>
        <source>Please enter your name:</source>
        <translation>Please enter your name:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="65"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="96"/>
        <source>Entire Lesson</source>
        <translation>Entire Lesson</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="184"/>
        <source>MMM d, yyyy hh:mm ap</source>
        <translation>MMM d, yyyy hh:mm ap</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="191"/>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="270"/>
        <location filename="../widget/lessonresult.cpp" line="465"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>You have reached %1 at a typing speed of %2 cpm and %n typing error.</numerusform>
            <numerusform>You have reached %1 at a typing speed of %2 cpm and %n typing errors.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="288"/>
        <location filename="../widget/lessonresult.cpp" line="483"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="299"/>
        <location filename="../widget/lessonresult.cpp" line="346"/>
        <location filename="../widget/lessonresult.cpp" line="495"/>
        <location filename="../widget/lessonresult.cpp" line="541"/>
        <source>Duration: </source>
        <translation>Duration: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="303"/>
        <location filename="../widget/lessonresult.cpp" line="499"/>
        <source>Typing Errors: </source>
        <translation>Typing Errors: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="307"/>
        <location filename="../widget/lessonresult.cpp" line="503"/>
        <source>Assistance: </source>
        <translation>Assistance: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="321"/>
        <location filename="../widget/lessonresult.cpp" line="516"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="338"/>
        <location filename="../widget/lessonresult.cpp" line="533"/>
        <source>Lesson: </source>
        <translation>Lesson: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="342"/>
        <location filename="../widget/lessonresult.cpp" line="537"/>
        <source>Time: </source>
        <translation>Time: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="350"/>
        <location filename="../widget/lessonresult.cpp" line="545"/>
        <source>Characters: </source>
        <translation>Characters: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="354"/>
        <location filename="../widget/lessonresult.cpp" line="549"/>
        <source>Errors: </source>
        <translation>Errors: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="358"/>
        <location filename="../widget/lessonresult.cpp" line="553"/>
        <source>Error Rate: </source>
        <translation>Error Rate: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="362"/>
        <location filename="../widget/lessonresult.cpp" line="557"/>
        <source>Cpm: </source>
        <translation>Cpm: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="376"/>
        <location filename="../widget/lessonresult.cpp" line="570"/>
        <source>Dictation</source>
        <translation>Dictation</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="459"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>TIPP10 Touch Typing Tutor</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="462"/>
        <source>Report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="462"/>
        <source> of %1</source>
        <translation> of %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="587"/>
        <source>Print Report</source>
        <translation>Print Report</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="87"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="92"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n character</numerusform>
            <numerusform>%n characters</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>Error Correction with Backspace</source>
        <translation>Error Correction with Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="106"/>
        <source>Error Correction without Backspace</source>
        <translation>Error Correction without Backspace</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="108"/>
        <source>Ignore Errors</source>
        <translation>Ignore Errors</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="116"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="124"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="132"/>
        <source>- Colored Keys</source>
        <translation>- Colored Keys</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="138"/>
        <source>- Home Row</source>
        <translation>- Home Row</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="144"/>
        <source>- Motion Paths</source>
        <translation>- Motion Paths</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="150"/>
        <source>- Separation Line</source>
        <translation>- Separation Line</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="156"/>
        <source>- Instructions</source>
        <translation>- Instructions</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="204"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="86"/>
        <source>MMM d, yyyy hh:mm ap</source>
        <translation>MMM d, yyyy hh:mm ap</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="91"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="94"/>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="116"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="160"/>
        <source>Show: </source>
        <translation>Show: </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="162"/>
        <source>All Lessons</source>
        <translation>All Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="163"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="164"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="165"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="315"/>
        <source>Lesson</source>
        <translation>Lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="316"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="317"/>
        <source>Duration</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="318"/>
        <source>Characters</source>
        <translation>Characters</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="319"/>
        <source>Errors</source>
        <translation>Errors</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="320"/>
        <source>Rate</source>
        <translation>Rate</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="321"/>
        <source>Cpm</source>
        <translation>Cpm</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="322"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="325"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>This column shows the names
of completed lessons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="329"/>
        <source>Start time of the lesson</source>
        <translation>Start time of the lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="331"/>
        <source>Total duration of the lesson</source>
        <translation>Total duration of the lesson</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="332"/>
        <source>Number of characters dictated</source>
        <translation>Number of characters dictated</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="335"/>
        <source>Number of typing errors</source>
        <translation>Number of typing errors</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="337"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="342"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="346"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="78"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>All results of the current lesson will be discarded!

Do you really want to exit?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="80"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="80"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="98"/>
        <location filename="../widget/mainwindow.cpp" line="120"/>
        <source>&amp;Go</source>
        <translation>&amp;Go</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="104"/>
        <location filename="../widget/mainwindow.cpp" line="124"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="138"/>
        <source>&amp;Settings</source>
        <translation>&amp;Settings</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="144"/>
        <source>&amp;General Settings</source>
        <translation>&amp;General Settings</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="146"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="148"/>
        <source>&amp;Results</source>
        <translation>&amp;Results</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="149"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manual</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="151"/>
        <source> on the web</source>
        <translation> on the web</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="153"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="155"/>
        <source>&amp;About </source>
        <translation>&amp;About </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="158"/>
        <source>ABC-Game</source>
        <translation>ABC-Game</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="211"/>
        <source>Software Version </source>
        <translation>Software Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="212"/>
        <source>Database Version </source>
        <translation>Database Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="217"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Intelligent Touch Typing Tutor</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="218"/>
        <source>On the web: </source>
        <translation>On the web: </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="220"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="232"/>
        <source>TIPP10 is published by</source>
        <translation>TIPP10 is published by</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="237"/>
        <source>About </source>
        <translation>About </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="214"/>
        <source>Portable Version</source>
        <translation>Portable Version</translation>
    </message>
</context>
<context>
    <name>OtherPage</name>
    <message>
        <location filename="../widget/settingspages.cpp" line="564"/>
        <source>Windows</source>
        <translation>Windows</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="567"/>
        <source>Show welcome message at startup</source>
        <translation>Show welcome message at startup</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="569"/>
        <source>Here you can decide if an information window with tips
is shown at the beginning of %1</source>
        <translation>Here you can decide if an information window with tips
is shown at the beginning of %1</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="575"/>
        <source>Notice prior to the import and export of lessons</source>
        <translation>Notice prior to the import and export of lessons</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="577"/>
        <source>Here you can determine whether a notice to
the file types should be displayed before the
import and export</source>
        <translation>Here you can determine whether a notice to
the file types should be displayed before the
import and export</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="583"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Remember enabled intelligence before starting
an open or own lesson</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="586"/>
        <source>Here you can define whether a warning window
is displayed when an open or own lesson with
active intelligence is started</source>
        <translation>Here you can define whether a warning window
is displayed when an open or own lesson with
active intelligence is started</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="592"/>
        <source>Reminder for the possibility of publishing an
own lesson as open lesson for all users</source>
        <translation>Reminder for the possibility of publishing an
own lesson as open lesson for all users</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="599"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="616"/>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="619"/>
        <source>Check for new version at startup once a week</source>
        <translation>Check for new version at startup once a week</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="621"/>
        <source>Use native user interface style</source>
        <translation>Use native user interface style</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="50"/>
        <location filename="../widget/progressionwidget.cpp" line="64"/>
        <location filename="../widget/progressionwidget.cpp" line="553"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="56"/>
        <source>Show: </source>
        <translation>Show: </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="58"/>
        <source>All Lessons</source>
        <translation>All Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="59"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="60"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="61"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="62"/>
        <source>Order by x-axis:</source>
        <translation>Order by x-axis:</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="65"/>
        <location filename="../widget/progressionwidget.cpp" line="559"/>
        <source>Lesson</source>
        <translation>Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="129"/>
        <source>MMM d, yyyy</source>
        <translation>MMM d, yyyy</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="130"/>
        <source>MMM d, yyyy hh:mm ap</source>
        <translation>MMM d, yyyy hh:mm ap</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="256"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="407"/>
        <location filename="../widget/progressionwidget.cpp" line="431"/>
        <source>Training Lesson</source>
        <translation>Training Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="414"/>
        <location filename="../widget/progressionwidget.cpp" line="438"/>
        <source>Open Lesson</source>
        <translation>Open Lesson</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="421"/>
        <location filename="../widget/progressionwidget.cpp" line="445"/>
        <source>Own Lesson</source>
        <translation>Own Lesson</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="504"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="505"/>
        <source>%1 cpm</source>
        <translation>%1 cpm</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="522"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>The progress graph will be shown after completing two lessons.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="564"/>
        <source>Cpm</source>
        <translation>Cpm</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="569"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="103"/>
        <source>TIPP10 found an old database file from version 1 in
your program directory.

Do you want to transfer the personal data from the
old database to the new database?
</source>
        <translation>Es wurde eine alte TIPP10-Datenbank der Version 1 im
Programmverzeichnis gefunden.

Wollen Sie die persönlichen Daten aus der alten Datenbank
in die neue Datenbank dieser Version übernehmen?
</translation>
    </message>
    <message>
        <location filename="../sql/connection.h" line="109"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../sql/connection.h" line="109"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../sql/connection.h" line="210"/>
        <location filename="../sql/connection.h" line="217"/>
        <location filename="../sql/connection.h" line="273"/>
        <location filename="../sql/connection.h" line="280"/>
        <location filename="../sql/connection.h" line="310"/>
        <source>Affected directory:
</source>
        <translation>Affected directory:
</translation>
    </message>
    <message>
        <location filename="../sql/connection.h" line="327"/>
        <source>Training Lesson</source>
        <translation>Training Lesson</translation>
    </message>
    <message>
        <location filename="../sql/connection.h" line="344"/>
        <source>Your data were successfully transfered
to the new database!
</source>
        <translation>Your data were successfully transfered
to the new database!
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="93"/>
        <source>en</source>
        <translation>en</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="142"/>
        <location filename="../sql/keyboardsql.cpp" line="48"/>
        <source>Left little finger</source>
        <translation>Left little finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="143"/>
        <location filename="../sql/keyboardsql.cpp" line="49"/>
        <source>Left ring finger</source>
        <translation>Left ring finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="144"/>
        <location filename="../sql/keyboardsql.cpp" line="50"/>
        <source>Left middle finger</source>
        <translation>Left middle finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="145"/>
        <location filename="../sql/keyboardsql.cpp" line="51"/>
        <source>Left forefinger</source>
        <translation>Left forefinger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="146"/>
        <location filename="../sql/keyboardsql.cpp" line="51"/>
        <source>Right forefinger</source>
        <translation>Right forefinger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="147"/>
        <location filename="../sql/keyboardsql.cpp" line="52"/>
        <source>Right middle finger</source>
        <translation>Right middle finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="148"/>
        <location filename="../sql/keyboardsql.cpp" line="53"/>
        <source>Right ring finger</source>
        <translation>Right ring finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="149"/>
        <location filename="../sql/keyboardsql.cpp" line="54"/>
        <source>Right little finger</source>
        <translation>Right little finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="149"/>
        <location filename="../sql/keyboardsql.cpp" line="54"/>
        <source>Thumb</source>
        <translation>Thumb</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="77"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="78"/>
        <source>&amp;Default</source>
        <translation>&amp;Default</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="91"/>
        <source>Limitation of characters</source>
        <translation>Limitation of characters</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="93"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="99"/>
        <source>Replacement Filter</source>
        <translation>Replacement Filter</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="101"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:</source>
        <translation>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="106"/>
        <source>ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>ä=ae,ö=oe,ü=ue,Ä=Ae,Ö=Oe,Ü=Ue,ß=ss</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.cpp" line="113"/>
        <source>Filter for the keyboard layout</source>
        <translation>Filter for the keyboard layout</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="65"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="66"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="67"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="97"/>
        <source>General Settings</source>
        <translation>General Settings</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="108"/>
        <source>Training</source>
        <translation>Training</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="114"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="120"/>
        <source>Results</source>
        <translation>Results</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="126"/>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="147"/>
        <source>Please wait</source>
        <translation>Please wait</translation>
    </message>
    <message>
        <location filename="../widget/settingsdialog.cpp" line="156"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Some of your settings require a restart of the software to take effect.
</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="232"/>
        <source> Zeichen</source>
        <translation> characters</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="217"/>
        <source> Minuten</source>
        <translation> minutes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="222"/>
        <source>Zeichenlimit:</source>
        <translation>Character Limit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="218"/>
        <source>Das Diktat wird nach Ablauf einer
festgelegten Zeit beendet</source>
        <translation>The dictation will be stopped after
a specified time period</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="224"/>
        <source>Das Diktat wird nach einer festgelegten Anzahl
korrekt getippter Zeichen beendet</source>
        <translation>The dictation will be stopped after
a specified number of characters</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="106"/>
        <location filename="../widget/startwidget.cpp" line="761"/>
        <source>Training Lessons</source>
        <translation>Training Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="113"/>
        <source>Subject:</source>
        <translation>Subject:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="165"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="169"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;New Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="171"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Import Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="173"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Export Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="175"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Edit Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="177"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Delete Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="206"/>
        <source>Duration of Lesson</source>
        <translation>Duration of Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="209"/>
        <source>Time Limit:</source>
        <translation>Time Limit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="211"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>The dictation will be stopped after
a specified time period</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="234"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>The dictation will be stopped after
a specified number of correctly typed
characters</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="238"/>
        <source>Entire
Lesson</source>
        <translation>Entire
Lesson</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="240"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>The complete lesson will be dictated
from beginning to end</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="241"/>
        <source>(Entire Lesson)</source>
        <translation>(Entire Lesson)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="272"/>
        <source>Response to Typing Errors</source>
        <translation>Response to Typing Errors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="275"/>
        <source>Block Typing Errors</source>
        <translation>Block Typing Errors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="277"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>The dictation will only proceed if the correct
key was pressed</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="280"/>
        <source>Correction with Backspace</source>
        <translation>Correction with Backspace</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="282"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Typing errors have to be removed
with the return key</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="285"/>
        <source>Audible Signal</source>
        <translation>Audible Signal</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="286"/>
        <source>A beep sounds with every typing error</source>
        <translation>A beep sounds with every typing error</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="289"/>
        <source>Show key picture</source>
        <translation>Show key picture</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="290"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>For every typing error the corresponding key picture is displayed on the keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="296"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="302"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="309"/>
        <source>Intelligence</source>
        <translation>Intelligence</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="311"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="344"/>
        <source>Assistance</source>
        <translation>Assistance</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="346"/>
        <source>Show Keyboard</source>
        <translation>Show Keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="347"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>For visual support, the virtual keyboard and
status information is shown</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="351"/>
        <source>Colored Keys</source>
        <translation>Colored Keys</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="353"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>For visual support pressing keys will be
marked with colors</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="356"/>
        <source>Home Row</source>
        <translation>Home Row</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="358"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>For visual support, the remaining fingers
of the home row will be colored</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="362"/>
        <source>L/R Separation Line</source>
        <translation>L/R Separation Line</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="364"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>For visual support a separation line between left
and right hand will be shown</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="368"/>
        <source>Instruction</source>
        <translation>Instruction</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="370"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Show fingers to be used in the status bar</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="373"/>
        <source>Motion Paths</source>
        <translation>Motion Paths</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="375"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Motion paths of the fingers will be shown
on the keyboard</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="423"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="424"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="425"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Start Training</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="554"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="773"/>
        <source>Open Lessons</source>
        <translation>Open Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="803"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="812"/>
        <source>Own Lessons</source>
        <translation>Own Lessons</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="932"/>
        <source>Please select a text file</source>
        <translation>Please select a text file</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="933"/>
        <source>Text files (*.txt)</source>
        <translation>Text files (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1056"/>
        <source>Please indicate the location of a text file</source>
        <translation>Please indicate the location of a text file</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1105"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1107"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1107"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="133"/>
        <source>Press space bar to proceed</source>
        <translation>Press space bar to proceed</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="150"/>
        <source>Dictation Finished</source>
        <translation>Dictation Finished</translation>
    </message>
</context>
<context>
    <name>TrainingPage</name>
    <message>
        <location filename="../widget/settingspages.cpp" line="78"/>
        <source>Ticker</source>
        <translation>Ticker</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="81"/>
        <source>Font:</source>
        <translation>Font:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="82"/>
        <source>Change &amp;Font</source>
        <translation>Change &amp;Font</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="84"/>
        <source>Here you can change the font of the ticker
(a font size larger than 20 pt is not recommended)</source>
        <translation>Here you can change the font of the ticker
(a font size larger than 20 pt is not recommended)</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="88"/>
        <source>Font Color:</source>
        <translation>Font Color:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="90"/>
        <source>Here you can select the font color</source>
        <translation>Here you can select the font color</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="93"/>
        <source>Background:</source>
        <translation>Background:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="96"/>
        <source>Here you can select the background color</source>
        <translation>Here you can select the background color</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="99"/>
        <source>Cursor:</source>
        <translation>Cursor:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="101"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Here can select the color of the cursor for the current character</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="105"/>
        <source>Speed:</source>
        <translation>Speed:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="106"/>
        <source>Fast</source>
        <translation>Fast</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="107"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="112"/>
        <source>Here you can change the speed of the ticker
(Slider on the left: Ticker does not move until reaching the end of line.
Slider on the right: The ticker moves very fast.)</source>
        <translation>Here you can change the speed of the ticker
(Slider on the left: Ticker does not move until reaching the end of line.
Slider on the right: The ticker moves very fast.)</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="160"/>
        <source>Audio Output</source>
        <translation>Audio Output</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="163"/>
        <source>Sound Card</source>
        <translation>Sound Card</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="165"/>
        <source>Select this option to hear the sound from the sound card</source>
        <translation>Select this option to hear the sound from the sound card</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="168"/>
        <source>PC Speaker</source>
        <translation>PC Speaker</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="170"/>
        <source>Select this option to hear the sound from the PC speaker</source>
        <translation>Select this option to hear the sound from the PC speaker</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="172"/>
        <source>Metronome:</source>
        <translation>Metronome:</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="173"/>
        <source>Here you can activate a metronome</source>
        <translation>Here you can activate a metronome</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="179"/>
        <source> cpm</source>
        <translation> cpm</translation>
    </message>
    <message>
        <location filename="../widget/settingspages.cpp" line="180"/>
        <source>Please select how often the metronome sound should appear per minute</source>
        <translation>Please select how often the metronome sound should appear per minute</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="129"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="136"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="142"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="244"/>
        <source>Press space bar to start</source>
        <translation>Press space bar to start</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="245"/>
        <source>Take Home Row position</source>
        <translation>Take Home Row position</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="261"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Do you really want to exit the lesson early?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="261"/>
        <location filename="../widget/trainingwidget.cpp" line="274"/>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="262"/>
        <location filename="../widget/trainingwidget.cpp" line="274"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="274"/>
        <source>Do you want to save your results?</source>
        <translation>Do you want to save your results?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="493"/>
        <source>E&amp;xit Lesson early</source>
        <translation>E&amp;xit Lesson early</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="556"/>
        <source>Errors: </source>
        <translation>Errors: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="558"/>
        <source>Cpm: </source>
        <translation>Cpm: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="559"/>
        <source>Time: </source>
        <translation>Time: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="561"/>
        <source>Characters: </source>
        <translation>Characters: </translation>
    </message>
</context>
</TS>
